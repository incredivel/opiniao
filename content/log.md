---
title: log
---

#### 2022-12

<details><summary>poetry  | Homero (Frederico Lourenço)  <b>Ilíada</b></summary><blockquote><a href="https://www.quetzaleditores.pt/produtos/ficha/iliada-de-homero/21327228">Quetzal</a></blockquote></details>

<details><summary>film | <b>Triangle of Sadness</b> (2022) Ruben Östlund</summary><blockquote><a href="https://en.wikipedia.org/wiki/Triangle_of_Sadness">Wikipedia</a></blockquote></details>

<details><summary>film | <b>Nymphomaniac</b> (2013) Lars von Trier</summary><blockquote><a href="https://en.wikipedia.org/wiki/Nymphomaniac_(film)">Wikipedia</a></blockquote></details>

<details><summary>film | <b>Antichrist</b> (2009) Lars von Trier</summary><blockquote><a href="https://en.wikipedia.org/wiki/Antichrist_(film)">Wikipedia</a></blockquote></details>

<details><summary>film | <b>Suspiria</b> (2018) Luca Guadagnino </summary><blockquote><a href="https://en.wikipedia.org/wiki/Suspiria_(2018_film)">Wikipedia</a></blockquote></details>

#### 2022-11

<details><summary>film | <b>The Gospel According to St. Matthew</b> (1964) Pier Paolo Pasolini</summary><blockquote><a href="https://www.criterionchannel.com/the-gospel-according-to-st-matthew">Criterion</a></blockquote></details>

<details><summary>exhi | MAAT – Museum of Art, Architecture and Technology</summary><blockquote><a href="https://maat.pt/en">MAAT</a></blockquote></details>

<details><summary>opera | <b>Don Giovanni</b> (Nuno Coelho) Mozart / Saramago </summary><blockquote><a href="https://gulbenkian.pt/musica/agenda/don-giovanni-2/">Gulbenkian</a></blockquote></details>

<details><summary>doc | <b>God Forbid</b> (2022) Billy Corben </summary><blockquote><a href="https://www.imdb.com/title/tt22695428/">IMDb</a></blockquote></details>

<details><summary>film | <b>Everything Everywhere All at Once</b> (2022) Daniel Kwan & Daniel Scheinert</summary><blockquote><a href="https://en.wikipedia.org/wiki/Everything_Everywhere_All_at_Once">Wikipedia</a></blockquote></details>

<details><summary>film | <b>Restos do Vento</b> (2022) Tiago Guedes </summary><blockquote><a href="https://www.imdb.com/title/tt14145004/">IMDb</a></blockquote></details>

#### 2022-10

<details><summary>play | <b>Ça ira (1) Fin de Louis</b> (Joël Pommerat) Marion Boudier </summary><blockquote><a href="https://www.tndm.pt/pt/calendario/ca-ira-1-fin-de-louis/">TNDM II</a></blockquote></details>

<details><summary>series | <b>Dekalog</b> (1988) Krzysztof Kieślowski</summary><blockquote><a href="https://en.wikipedia.org/wiki/Dekalog">Wikipedia</a></blockquote></details>

<details><summary>play | <b>Naquele dia, não passou na televisão</b> (Joana Craveiro) Teatro do Vestido</summary><blockquote><a href="https://www.museudoaljube.pt/evento/naquele-dia-nao-passou-na-televisao/">Museu do Aljube</a></blockquote></details>

<details><summary>doc | <b>They Shall Not Grow Old</b> (2018) Peter Jackson </summary><blockquote><a href="https://en.wikipedia.org/wiki/They_Shall_Not_Grow_Old">Wikipedia</a></blockquote></details>

<details><summary>nonfic | Hardy, Godfrey Harold (1940) <b>A Mathematician's Apology</b> </summary><blockquote>@book{hardy2017apology,
  title = {A Mathematician's Apology},
  author = {Hardy, Godfrey Harold},
  date = {2017},
  series = {Canto Classics},
  publisher = {{Cambridge University Press}},
  isbn = {978-1-107-60463-6 1-107-60463-X},
}
</blockquote></details>

<details><summary>nonfic | Rodrik, Dani (2015) <b>Economics Rules</b></summary><blockquote>@book{rodrik2015rules,
  title = {Economics Rules: {{The}} Rights and Wrongs of the Dismal Science},
  author = {Rodrik, Dani},
  date = {2015},
  edition = {1},
  publisher = {{W. W. Norton \& Co.}},
  isbn = {0-393-24641-8 978-0-393-24641-4},
}
</blockquote></details>

<details><summary>doc | <b>United States of Secrets</b> (2014) Frontline</summary><blockquote><a href="https://www.pbs.org/wgbh/frontline/documentary/united-states-of-secrets/">PBS</a></blockquote></details>

<details><summary>series | <b>The Lincoln Project</b> (2022) Karim Amer </summary><blockquote><a href="https://en.wikipedia.org/wiki/The_Lincoln_Project_(TV_series)">Wikipedia</a></blockquote></details>

<details><summary>doc | <b>JFK Revisited: Through the Looking Glass</b> (2021) Oliver Stone</summary><blockquote><a href="https://en.wikipedia.org/wiki/JFK_Revisited%3A_Through_the_Looking_Glass">Wikipedia</a></blockquote></details>

#### 2022-09

<details><summary>doc | <b>The Labor Files</b> (2022) Al Jazeera</summary><blockquote><a href="https://www.ajiunit.com/investigation/the-labour-files/">Al Jazeera</a></blockquote></details>

<details><summary>play | <b>Casa Portuguesa</b> (Pedro Penim) Pedro Penim </summary><blockquote><a href="https://www.tndm.pt/pt/calendario/casa-portuguesa/">TNDM II</a></blockquote></details>

<details><summary>opera | <b>Zelle</b> (Jamie Man) Jamie Man</summary><blockquote><a href="https://gulbenkian.pt/en/agenda/zelle/">Gulbenkian</a></blockquote></details>

<details><summary>film | <b>Pickpocket</b> (1959) Robert Bresson </summary><blockquote><a href="https://en.wikipedia.org/wiki/Pickpocket_(film)">Wikipedia</a></blockquote></details>

<details><summary>film | <b>Viaggio In Italia</b> (1954) Roberto Rossellini </summary><blockquote><a href="https://en.wikipedia.org/wiki/Journey_to_Italy">Wikipedia</a></blockquote></details>

<details><summary>series | <b>The Rehearsal</b> (2022) Nathan Fielder </summary><blockquote><a href="https://en.wikipedia.org/wiki/The_Rehearsal_(TV_series)">Wikipedia</a></blockquote></details>

#### 2022-08

<details><summary>course | <b>Unsupervised Learning, Recommenders, Reinforcement Learning</b> (Stanford) Andrew Ng</summary><blockquote><a href="https://www.coursera.org/learn/unsupervised-learning-recommenders-reinforcement-learning/">Coursera</a></blockquote></details>

<details><summary>nonfic | Pessoa, Fernando (2015) <b>Prosa crítica e ensaística</b></summary><blockquote>
@book{pessoa2015critica,
   title =     {Prosa crítica e ensaística},
   author =    {Fernando Pessoa},
   publisher = {Alêtheia Editores},
   isbn =      {978-989-99376-4-2},
   year =      {2015}}
</blockquote></details>

<details><summary>course | <b>Advanced Learning Algorithms</b> (Stanford) Andrew Ng</summary><blockquote><a href="https://www.coursera.org/learn/advanced-learning-algorithms">Coursera</a></blockquote></details>

<details><summary>nonfic | Lourenço, Eduardo (1997) <b>Nós como Futuro</b></summary><blockquote>
@book{lourenco1997futuro,
   title =     {Nós como Futuro},
   author =    {Eduardo Lourenço},
   publisher = {Assírio & Alvim},
   year =      {1997}}
</blockquote></details>

<details><summary>poetry | Mendonça, José Tolentino (2021) <b>Introdução à Pintura Rupestre</b></summary><blockquote>
@book{mendonca2021rupestre,
   title =     {Introdução à Pintura Rupestre},
   author =    {José Tolentino Mendonça},
   publisher = {Assírio & Alvim},
   isbn =      {978-972-37-2211-6},
   year =      {2021}}
</blockquote></details>

<details><summary>course | <b>Supervised Machine Learning: Regression and Classification</b> (Stanford) Andrew Ng</summary><blockquote><a href="https://www.coursera.org/learn/machine-learning">Coursera</a></blockquote></details>

<details><summary>fic | de Sena, Jorge (1979) <b>Sinais de Fogo</b></summary><blockquote>
@book{sena1979fogo,
   title =     {Sinais de Fogo},
   author =    {Jorge de Sena},
   publisher = {Livros do Brasil},
   isbn =      {978-972-38-3006-4},
   year =      {1979}}</blockquote></details>

<details><summary>course | <b>Introduction to Philosophy</b> (The University of Edinburgh)</summary><blockquote><a href="https://www.coursera.org/learn/philosophy">Coursera</a></blockquote></details>

<details><summary>film | <b>Cléo from 5 to 7</b> (1962) Agnès Vardas</summary><blockquote><a href="https://www.criterionchannel.com/cleo-from-5-to-7-1">Criterion</a></blockquote></details>

<details><summary>film | <b>Three Colors: Red</b> (1994) Krzysztof Kieślowski</summary><blockquote><a href="https://www.criterionchannel.com/three-colors-red">Criterion</a></blockquote></details>

<details><summary>film | <b>Three Colors: White</b> (1994) Krzysztof Kieślowski</summary><blockquote><a href="https://www.criterionchannel.com/three-colors-white">Criterion</a></blockquote></details>

<details><summary>film | <b>Three Colors: Blue</b> (1993) Krzysztof Kieślowski</summary><blockquote><a href="https://www.criterionchannel.com/three-colors-blue">Criterion</a></blockquote></details>

<details><summary>film | <b>Viridiana</b> (1961) Luis Buñuel</summary><blockquote><a href="https://www.criterionchannel.com/viridiana">Criterion</a></blockquote></details>

<details><summary>film | <b>Knife in the Water</b> (1962) Roman Polanski</summary><blockquote><a href="https://www.criterion.com/films/219-knife-in-the-water">Criterion</a></blockquote></details>

<details><summary>doc | <b>The Art of Piano</b> (1999) Donald Sturrock</summary><blockquote><a href="https://www.imdb.com/title/tt2376902/">IMDb</a></blockquote></details>

<details><summary>film | <b>Cinema Paradiso</b> (1988) Giuseppe Tornatore</summary><blockquote><a href="https://www.criterionchannel.com/cinema-paradiso">Criterion</a></blockquote></details>

<details><summary>film | <b>The Worst Person in the World</b> (2021) Joachim Trier</summary><blockquote><a href="https://en.wikipedia.org/wiki/The_Worst_Person_in_the_World_(film)">Wikipedia</a></blockquote></details>

<details><summary>course | <b>How to Speak</b> (MIT) Patrick Winston</summary><blockquote><a href="https://ocw.mit.edu/courses/res-tll-005-how-to-speak-january-iap-2018/pages/how-to-speak/">MIT</a></blockquote></details>

<details><summary>film | <b>Jazz on a Summer's Day</b> (1959) Bert Stern and Aram Avakian</summary><blockquote><a href="https://www.criterionchannel.com/jazz-on-a-summer-s-day">Criterion</a></blockquote></details>

<details><summary>film | <b>The Celebration</b> (1998) Thomas Vinterberg</summary><blockquote><a href="https://www.criterionchannel.com/the-celebration">Criterion</a></blockquote></details>

<details><summary>short | <b>Black Panthers</b> (1970) Agnès Varda</summary><blockquote><a href="https://www.criterionchannel.com/black-panthers">Criterion</a></blockquote></details>

<details><summary>film | <b>Frances Ha</b> (2013) Noah Baumbach</summary><blockquote><a href="https://www.criterionchannel.com/frances-ha">Criterion</a></blockquote></details>

<details><summary>film | <b>The Daytrippers</b> (1996) Greg Mottola</summary><blockquote><a href="https://www.criterionchannel.com/the-daytrippers">Criterion</a></blockquote></details>

<details><summary>film | <b>Joker</b> (2019) Todd Phillips</summary><blockquote><a href="https://en.wikipedia.org/wiki/Joker_(2019_film)">Wikipedia</a></blockquote></details>

<details><summary>film | <b>Taxi Driver</b> (1976) Martin Scorsese</summary><blockquote><a href="https://en.wikipedia.org/wiki/Taxi_Driver">Wikipedia</a></blockquote></details>

<details><summary>film | <b>Personal Shopper</b> (2016) Olivier Assayas </summary><blockquote><a href="https://en.wikipedia.org/wiki/Personal_Shopper">Wikipedia</a></blockquote></details>

<details><summary>fic | M. Tavares, Gonçalo (2008) <b>O Senhor Breton e a entrevista</b></summary><blockquote>
@book{tavares2008breton,
  title = {O Senhor Breton e a entrevista},
  author = {Gonçalo M. Tavares},
  publisher = {Editorial Caminho},
  isbn = {978-972-21-2014-2},
  year = {2008}
}
</blockquote></details>

<details><summary>film | <b>Melancholia</b> (2011) Lars von Trier</summary><blockquote><a href="https://en.wikipedia.org/wiki/Melancholia_(2011_film)">Wikipedia</a></blockquote></details>


#### 2022-07

<details><summary>poetry | Verde, Cesário (2021) <b>O Livro de Cesário Verde e Outros Poemas</b> </summary><blockquote>
@book{verde2021cesario,
  title = {O Livro de Cesário Verde e Outros Poemas},
  author = {Cesário Verde},
  publisher = {Penguin Clássicos},
  isbn = {9789897843334},
  year = {2021}
}
</blockquote></details>

<details><summary>fic | Tiago, Manuel (1975) <b>Cinco Dias, Cinco Noites</b></summary><blockquote>
@book{tiago1975cinco,
  title = {Cinco Dias, Cinco Noites},
  author = {Manuel Tiago},
  publisher = {Editorial Avante},
  isbn = {9789725502334},
  year = {1975}
}
</blockquote></details>

<details><summary>film | <b>Manchester by the Sea</b> (2016) Kenneth Lonergan</summary><blockquote><a href="https://en.wikipedia.org/wiki/Manchester_by_the_Sea_(film)">Wikipedia</a></blockquote></details>

<details><summary>doc | <b>Endangered</b> (2022) Heidi Ewing</summary><blockquote><a href="https://yewtu.be/watch?v=vRLCqi5hodM">Trailer</a></blockquote></details>

#### 2022-06

<details><summary>nonfic | Mazzucato, Mariana (2015) <b>The Entrepreneurial State</b> </summary><blockquote>@book{mazzuacato2015entrepreneurial,
  title = {The Entrepreneurial State: Debunking Public vs. Private Sector Myths},
  author = {Mazzucato, Mariana},
  publisher = {PublicAffairs},
  isbn = {9781610396141,9781610396134,1610396138,1610396146},
  year = {2015},
  series = {},
  edition = {Revised edition},
}

<a href="https://en.wikipedia.org/wiki/The_Entrepreneurial_State">Wikipedia</a>
</blockquote></details>

<details><summary>nonfic | Graeber, David (2021) <b>The Dawn of Everything</b></summary><blockquote>@book{graeber2021everything,
  title = {The Dawn of Everything: A New History of Humanity},
  author = {David Graeber and David Wengrow},
  publisher = {Farrar, Straus and Giroux},
  isbn = {0374157359,9780374157357},
  year = {2021},
}

<a href="https://en.wikipedia.org/wiki/The_Dawn_of_Everything">Wikipedia</a>
</blockquote></details>

<details><summary>play | <b>A Sagração da Primavera</b> (Stravinsky) Teatro Praga</summary><blockquote><a href="https://www.ccb.pt/evento/teatro-praga-metropolitana/2022-06-18/">CCB</a></blockquote></details>

<details><summary>doc | <b>Ways of Seeing</b> (1972) John Berger</summary><blockquote><a href="https://en.wikipedia.org/wiki/Ways_of_Seeing">Wikipedia</a></blockquote></details>

<details><summary>play | <b>Outra Lingua</b> (Keli Freitas) Raquel André</summary><blockquote><a href="https://www.tndm.pt/pt/digressoes/outra-lingua/">TNDM II</a></blockquote></details>

<details><summary>doc | <b>Moonlight</b> (2016) Barry Jenkins</summary><blockquote><a href="https://en.wikipedia.org/wiki/Moonlight_(2016_film)">Wikipedia</a></blockquote></details>

#### 2022-05

<details><summary>film | <b>Blue Valentine</b> (2010) Derek Cianfrance</summary><blockquote><a href="https://en.wikipedia.org/wiki/Blue_Valentine_(film)">Wikipedia</a></blockquote></details>

<details><summary>film | <b>Drive</b> (2011) Nicolas Winding Refn</summary><blockquote><a href="https://en.wikipedia.org/wiki/Drive_(2011_film)">Wikipedia</a></blockquote></details>

<details><summary>poetry | Martelo, Rosa (2020) <b>Antologia Dialogante de Poesia Portuguesa</b></summary><blockquote><a href="https://www.assirio.pt/produtos/ficha/antologia-dialogante-de-poesia-portuguesa/24000418">Assírio & Alvim</a></blockquote></details>

<details><summary>doc | <b>O Jovem Cunhal</b> (2022) João Botelho</summary><blockquote><a href="https://indielisboa.com/en/movies/young-mr-cunhal/">IndieLisboa 2022</a></blockquote></details>

#### 2022-04

<details><summary>nonfic | Joque, Justin (2022) <b>Revolutionary Mathematics</b> </summary><blockquote><a href="https://www.versobooks.com/books/3897-revolutionary-mathematics">Verso</a></blockquote></details>

<details><summary>doc | <b>Can't Get You Out of My Head</b> (2021) Adam Curtis</summary><blockquote><a href="https://en.wikipedia.org/wiki/Can't_Get_You_Out_of_My_Head_(TV_series)">Wikipedia</a></blockquote></details>

<details><summary>doc | <b>The Century of the Self</b> (2002) Adam Curtis</summary><blockquote><a href="https://en.wikipedia.org/wiki/The_Century_of_the_Self">Wikipedia</a></blockquote></details>

<details><summary>film | <b>Paisan</b> (1946) Roberto Rossellini</summary><blockquote><a href="https://www.criterionchannel.com/videos/paisan">Criterion</a></blockquote></details>

<details><summary>film | <b>Rome Open City</b> (1945) Roberto Rossellini</summary><blockquote><a href="https://www.criterionchannel.com/videos/rome-open-city">Criterion</a></blockquote></details>

<details><summary>doc | <b>In the Same Breath</b> (2021) Nanfu Wang</summary><blockquote><a href="https://en.wikipedia.org/wiki/In_the_Same_Breath">Wikipedia</a></blockquote></details>

<details><summary>doc | <b>Ascension</b> (2021) Jessica Kingdon</summary><blockquote><a href="https://en.wikipedia.org/wiki/Ascension_(film)">Wikipedia</a></blockquote></details>

#### 2022-03

<details><summary>doc | <b>Cameraperson</b> (2016) Kirsten Johnson</summary><blockquote><a href="https://en.wikipedia.org/wiki/Cameraperson">Wikipedia</a></blockquote></details>


<details><summary>doc | <b>All Light, Everywhere</b> (2021) Theo Anthony</summary><blockquote><a href="https://en.wikipedia.org/wiki/All_Light%2C_Everywhere">Wikipedia</a></blockquote></details>


#### 2022-02

<details><summary>doc | <b>Collective</b> (2019) Alexander Nanau</summary><blockquote><a href="https://en.wikipedia.org/wiki/Collective_(2019_film)">Wikipedia</a></blockquote></details>

<details><summary>opera | <b>Rigoletto</b> (Bartlett Sher) Verdi</summary><blockquote><a href="https://gulbenkian.pt/agenda/rigoletto/">Gulbenkian</a></blockquote></details>

<details><summary>play | <b>Paraíso</b> (Dante Alighieri) João Brites</summary><blockquote><a href="https://www.tndm.pt/pt/espetaculos/paraiso-a-divina-comedia/">TNDM II</a></blockquote></details>

<details><summary>film | <b>The Children Are Watching Us</b> (1944) Vittorio De Sica</summary><blockquote><a href="https://www.criterionchannel.com/the-children-are-watching-us">Criterion</a></blockquote></details>

#### 2022-01

<details><summary>film | <b>A Metamorfose dos Pássaros</b> (2020) Catarina Vasconcelos</summary><blockquote><a href="https://www.rtp.pt/programa/tv/p41745">RTP</a></blockquote></details>

<details><summary>film | <b>O Ano da Morte de Ricardo Reis</b> (2020) João Botelho</summary><blockquote><a href="https://www.rtp.pt/programa/tv/p41352">RTP</a></blockquote></details>


#### 2021-12

<details><summary>fic | Brandão, Raul (1926) <b>A Morte do Palhaço e o Mistério da Árvore</b></summary><blockquote>@book{brandao1926morte,
  title = {A morte do palha{\c{c}}o e o mist{\'e}rio da {\'a}rvore},
  author = {Brand{\~a}o, Raul},
  year = {1926},
}
</blockquote></details>

<details><summary>nonfic | Mann, Geoff (2017) <b>In the Long Run We Are All Dead</b></summary><blockquote>@book{mann2017keynesianism,
  title = {In the Long Run We Are All Dead: Keynesianism, Political Economy, and Revolution},
  author = {Geoff Mann},
  publisher = {Verso},
  isbn = {1784785997,9781784785994},
  year = {2017},
}

<a href="https://www.versobooks.com/books/3000-in-the-long-run-we-are-all-dead">Verso</a>
</blockquote></details>

<details><summary>nonfic | Mann, Geoff (2017) <b>The General Theory of Employment, Interest and Money: A Reader's Companion</b></summary><blockquote>@book{mann2017companion,
  title = {The General Theory of Employment, Interest and Money: A Reader's Companion},
  author = {Geoff Mann},
  year = {2017},
  publisher = {Verso Books},
  isbn = {9781786634528},
}

<a href="https://www.versobooks.com/books/2499-the-general-theory-of-employment-interest-and-money">Verso</a>
</blockquote></details>

<details><summary>film | <b>Reds</b> (1981) Warren Beatty</summary><blockquote><a href="https://en.wikipedia.org/wiki/Reds_(film)">Wikipedia</a></blockquote></details>

<details><summary>nonfic | Collective, The Care (2020) <b>The Care Manifesto</b></summary><blockquote>@book{collective2020care,
  title = {The Care Manifesto},
  author = {The Care Collective and Jamie Hakim and Jo Littler and Catherine Rottenberg and Lynne Segal},
  publisher = {Verso Books},
  year = {2020},
  isbn = {9781839760983,9781839760976},
}

<a href="https://www.versobooks.com/books/3706-care-manifesto">Verso</a>
</blockquote></details>

<details><summary>nonfic | Mair, Peter (2013) <b>Ruling the Void</b></summary><blockquote>@book{mair2013void,
  title = {Ruling the void: the hollowing of western democracy},
  author = {Mair, Peter},
  publisher = {Verso Books},
  isbn = {9781781682340,1781682348},
  year = {2014;2013},
}

<a href="https://www.versobooks.com/books/1447-ruling-the-void">Verso</a>
</blockquote></details>

#### 2021-11

<details><summary>film | <b>Equinox Flower</b> (1958) Yasujiro Ozu</summary><blockquote><a href="https://www.criterionchannel.com/equinox-flower">Criterion</a></blockquote></details>

<details><summary>nonfic | Collective, T. S. (2021) <b>The Tragedy of the Worker</b> </summary><blockquote>@book{salvage2021worker,
  title = {The Tragedy of the Worker},
  author = {The Salvage Collective},
  publisher = {Verso Books},
  isbn = {9781839762963,9781839762949,9781839762956,2021937570},
  year = {2021},
}

<a href="https://www.versobooks.com/books/3727-the-tragedy-of-the-worker">Verso</a>
</blockquote></details>

<details><summary>film | <b>Coffee and Cigarettes</b> (2003) Jim Jarmusch</summary><blockquote><a href="https://www.criterionchannel.com/coffee-and-cigarettes/videos/coffee-and-cigarettes">Criterion</a></blockquote></details>

<details><summary>series | <b>Scenes from a Marriage</b> (1973) Ingmar Bergman</summary><blockquote><a href="https://www.criterionchannel.com/scenes-from-a-marriage-television-version">Criterion</a></blockquote></details>

<details><summary>nonfic | Anderson, Perry (2021) <b>Ever Closer Union?</b></summary><blockquote>@book{anderson2021ever,
  title = {Ever Closer Union?: Europe in the West},
  author = {Anderson, Perry},
  year = {2021},
  publisher = {Verso Books},
}

<a href="https://www.versobooks.com/books/3883-ever-closer-union">Verso</a>
</blockquote></details>

<details><summary>short | <b>What Is a Woman?</b> (2020) Marin Håskjold</summary><blockquote><a href="https://www.criterionchannel.com/what-is-a-woman">Criterion</a></blockquote></details>

#### 2021-10

<details><summary>film | <b>One Cut of the Dead</b> (2017) Shin'ichirō Ueda</summary><blockquote><a href="https://en.wikipedia.org/wiki/One_Cut_of_the_Dead">Wikipedia</a></blockquote></details>

<details><summary>film | <b>Lamb</b> (2021) Valdimar Jóhannsson</summary><blockquote><a href="https://en.wikipedia.org/wiki/Lamb_(2021_film)">Wikipedia</a></blockquote></details>

<details><summary>nonfic | Cesaratto, Sergio (2020) <b>Heterodox challenges in economics</b></summary><blockquote>@book{cesaratto2020heterodox,
  title = {Heterodox Challenges in Economics: Theoretical Issues and the Crisis of the Eurozone},
  author = {Sergio Cesaratto},
  publisher = {Springer International Publishing;Springer},
  isbn = {9783030544478,9783030544485},
  year = {2020},
  series = {},
  edition = {1st ed.},
}</blockquote></details>

#### 2021-09

<details><summary>nonfic | Valdaliso, Covadonga (2021) <b>Museus de Lisboa</b> </summary><blockquote><a href="https://www.ffms.pt/publicacoes/detalhe/5412/museus-de-lisboa">FFMS</a></blockquote></details>

<details><summary>nonfic | Vieria, Pedro (2021) <b>Em que posso ser útil</b></summary><blockquote><a href="https://www.ffms.pt/publicacoes/detalhe/5687/em-que-posso-ser-util">FFMS</a></blockquote></details>

<details><summary>film | <b>Close-up</b> (1990) Abbas Kiarostami </summary><blockquote><a href="https://www.criterionchannel.com/close-up">Criterion</a></blockquote></details>

<details><summary>film | <b>Through the Olive Trees</b> (1994) Abbas Kiarostami</summary><blockquote><a href="https://www.criterionchannel.com/videos/through-the-olive-trees">Criterion</a></blockquote></details>

<details><summary>film | <b>And Life Goes On</b> (1992) Abbas Kiarostami</summary><blockquote><a href="https://www.criterionchannel.com/videos/and-life-goes-on">Criterion</a></blockquote></details>

<details><summary>film | <b>Where Is the Friend’s House?</b> (1987) Abbas Kiarostami</summary><blockquote><a href="href="https://www.criterionchannel.com/videos/where-is-the-friend-s-house">Criterion</a></blockquote></details>

#### 2021-08

<details><summary>film | <b>Wings of Desire</b> (1987) Wim Wenders</summary><blockquote><a href="https://www.criterion.com/films/200-wings-of-desire">Criterion</a></blockquote></details>

<details><summary>film | <b>The Seventh Seal</b> (1957) Ingmar Bergman</summary><blockquote><a href="https://www.criterion.com/films/173-the-seventh-seal">Criterion</a></blockquote></details>

<details><summary>film | <b>Vivre sa vie</b> (1962) Jean-Luc Godard</summary><blockquote><a href="https://www.criterionchannel.com/vivre-sa-vie">Criterion</a></blockquote></details>

<details><summary>film | <b>8½</b> (1963) Federico Fellini</summary><blockquote><a href="https://www.criterionchannel.com/81-2">Criterion</a></blockquote></details>

<details><summary>film | <b>The Battle of Algiers</b> (1966) Gillo Pontecorvo</summary><blockquote><a href="https://www.criterion.com/films/248-the-battle-of-algiers">Criterion</a></blockquote></details>

<details><summary>film | <b>Bicycle Thieves</b> (1948) Vittorio De Sica</summary><blockquote><a href="https://www.criterion.com/films/210-bicycle-thieves">Criterion</a></blockquote></details>

<details><summary>film | <b>Stalker</b> (1979) Andrei Tarkovsky</summary><blockquote><a href="https://www.criterion.com/films/28150-stalker">Criterion</a></blockquote></details>

<details><summary>exhi | Museu Coleção Berardo</summary><blockquote><a href="https://pt.museuberardo.pt/">Website</a></blockquote></details>

#### 2021-06

<details><summary>film | <b>What Happened Was</b> (1994) Tom Noonan</summary><blockquote><a href="https://www.criterionchannel.com/what-happened-was">Criterion</a></blockquote></details>

<details><summary>film | <b>Ossos</b> (1997) Pedro Costa</summary><blockquote><a href="https://en.wikipedia.org/wiki/Ossos">Wikipedia</a></blockquote></details>

<details><summary>fic | Sena, Jorge de (1977) <b>O Físico Prodigioso</b></summary><blockquote>@book{sena1977prodigioso,
   title =     {O Físico Prodigioso},
   author =    {Sena, Jorge de},
   publisher = {SL},
   year =      {1977}}</blockquote></details>

<details><summary>course | <b>Introduction to Political Philosophy</b> (Yale) Steven B. Smith</summary><blockquote><a href="https://oyc.yale.edu/political-science/plsc-114">Yale</a></blockquote></details>

#### 2021-05

<details><summary>film | <b>Solaris</b> (1972) Andrei Tarkovsky</summary><blockquote><a href="https://en.wikipedia.org/wiki/Solaris_%281972_film%29">Wikipedia</a></blockquote></details>

<details><summary>course | <b>The American Novel Since 1945</b> (Yale) Amy Hungerford </summary><blockquote><a href="https://oyc.yale.edu/english/engl-291">Yale</a></blockquote></details>

<details><summary>poetry | Horace (19BCE) <b>Ars Poetica</b></summary><blockquote>@book{horacio65ars,
   title = {Arte poética},
   author = {Horácio and Fernandes, Raúl Miguel Rosado},
   publisher = {Livraria Clássica},
   year = {196?}}

<a href="https://en.wikipedia.org/wiki/Ars_Poetica_%28Horace%29">Wikipedia</a></blockquote></details>

#### 2021-04

<details><summary>series | <b>How To with John Wilson</b> (2020) John Wilson</summary><blockquote><a href="https://en.wikipedia.org/wiki/How_To_with_John_Wilson">Wikipedia</a></blockquote></details>

<details><summary>film | <b>Andrei Rublev</b> (1966) Andrei Tarkovsky</summary><blockquote><a href="https://www.criterionchannel.com/videos/andrei-rublev">Criterion</a></blockquote></details>

<details><summary>film | <b>Ivan's Childhood</b> (1962) Andrei Tarkovsky</summary><blockquote><a href="https://www.criterionchannel.com/videos/ivan-s-childhood">Criterion</a></blockquote></details>

<details><summary>play | <b>O Dicionário da Fé</b> (Gonçalo M. Tavares) Jean Paul Bucchieri</summary><blockquote><a href="https://www.tndm.pt/pt/espetaculos/o-dicionario-da-fe/">TNDM II</a></blockquote></details>

<details><summary>doc | <b>Rat Film</b> (2016) Theo Anthony</summary><blockquote><a href="https://en.wikipedia.org/wiki/Rat_Film">Wikipedia</a></blockquote></details>

#### 2021-03

<details><summary>doc | <b>Boys State</b> (2020) Jesse Moss and Amanda McBaine</summary><blockquote><a href="https://en.wikipedia.org/wiki/Boys_State_(film)">Wikipedia</a></blockquote></details>

<details><summary>doc | <b>Mão Invisível</b> (2020) João Ramos de Almeida</summary><blockquote><a href="https://saladeimprensa.ces.uc.pt/index.php?col=canalces&id=28494#.XmZRTkPgp0s">YouTube</a></blockquote></details>

<details><summary>nonfic | Lourenço, Eduardo (1973) <b>Fernando Pessoa revisitado</b></summary><blockquote>@book{lourenco1973revisitado,
   title =     {Fernando Pessoa revisitado},
   author =    {Eduardo Louren\c{c}o},
   publisher = {Inova},
   year =      {1973}}</blockquote></details>

<details><summary>fiction | <b>Gilgamesh</b></summary><blockquote>@book{tamen2008gilgamesh,
   title =     {Gilgamesh},
   author =    {Pedro Támen and Nancy K. Sandars and Luís Alves da Costa},
   publisher = {Vega},
   isbn =      {9789726992271},
   year =      {2007},
   edition =   {4}}</blockquote></details>

#### 2021-02

<details><summary>doc | <b>Anos de Guerra - Guiné 1963-1974</b> (2000) José Barahona</summary><blockquote><a href="https://www.youtube.com/watch?v=mfSwSzRl9bM">YouTube</a></blockquote></details>

<details><summary>doc | <b>As Constituições</b> (2017) Rui Pinto de Almeida</summary><blockquote><a href="https://www.rtp.pt/programa/tv/p35061">RTP</a></blockquote></details>

<details><summary>film | <b>Blowup</b> (1966) Michelangelo Antonioni</summary><blockquote><a href="https://en.wikipedia.org/wiki/Blowup">Wikipedia</a></blockquote></details>

<details><summary>film | <b>I, Daniel Blake</b> (2016) Ken Loach</summary><blockquote><a href="https://en.wikipedia.org/wiki/I,_Daniel_Blake">Wikipedia</a></blockquote></details>

#### 2021-01

<details><summary>fiction | Voltaire (1759) <b>Candide, ou l'Optimisme</b></summary><blockquote>@book{voltaire2006candide,
   title =     {Candide and Other Stories},
   author =    {Voltaire and Roger Pearson},
   publisher = {Oxford University Press, USA},
   isbn =      {9780192807267,0192807269},
   year =      {2006},
   series =    {Oxford World's Classics}}</blockquote></details>

<details><summary>fiction | Cortázar, Julio (1968) <b>Blow-up and Other Stories</b></summary><blockquote>@book{cortazar2014blow,
   title =     {Blow-Up: And Other Stories},
   author =    {Julio Cortazar and Paul Julio Blackburn (translator)},
   publisher = {Knopf Doubleday Publishing Group},
   isbn =      {0-394-72881-5,978-0-8041-5324-9},
   year =      {2014}}</blockquote></details>
