---
title: "citações com zotero"
date: 2022-09-10T22:52:13+01:00
---

O meu universo de referências é suportado pelo [GNU Emacs](https://www.gnu.org/software/emacs/) e o [Zotero](https://www.zotero.org/). Este apontamento elucidativo assume a configuração funcional do Emacs feita com o helm-bibtex/org-noter/org-roam-bibtex.

A lógica de utilização do Zotero passa por: 1) encontrar o artigo na internet, 2) carregar no botão da extensão, 3) garantir que o pdf foi adicionado ao programa, e 4) começar a tirar notas com o org-noter. 

Deixámos a pasta-mãe em `~/zotero/` e permitimos que a aplicação seja atualizada com os servidores próprios do serviço. Centralmente, configurámos a «Linked Attachment Base Directory» com a pasta dos documentos que será atualizada via git, em `~/org/bibliotheca/`.

Esta solução só é possível graças ao [Better BibTeX](https://retorque.re/zotero-better-bibtex/) e ao [Zotfile](http://zotfile.com/). O primeiro é útil para exportar a biblioteca num só ficheiro `references.bib` [^config], que é usado pelo helm-bibtex. O segundo ajuda a renomear os documentos descarregados de modo a que mantenham a chave bibliográfica como nome, deixando o org-roam-bibtex fazer as notas em `~/org/notes/` com os mesmos nomes.

[^config]: Em `about:config`, passei `extensions.zotero.saveRelativeAttachmentPath` para verdadeiro. Assim, cada entrada da bibliografia não mostra o caminho absoluto para cada ficheiro (e.g., `/home/<user>/org/bibliotheca/author2021book.pdf`).
